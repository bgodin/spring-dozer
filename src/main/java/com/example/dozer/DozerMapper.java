package com.example.dozer;

import com.example.dozer.domain.ClassA;
import com.example.dozer.domain.ClassB;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DozerMapper {

    @Autowired
    private Mapper mapper;

    public ClassB mapAtoB(final ClassA a) {
        return mapper.map(a, ClassB.class);
    }

}