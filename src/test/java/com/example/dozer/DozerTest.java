package com.example.dozer;

import com.example.dozer.config.AppConfig;
import com.example.dozer.domain.ClassA;
import com.example.dozer.domain.ClassA1;
import com.example.dozer.domain.ClassB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class DozerTest{

    @Autowired
    DozerMapper dozerMapper;

    @Test
    public void dozerTest(){
        ClassA a = new ClassA();
        a.setStringField("aaa");
        a.setBooleanField(false);
        a.setLongField(100L);
        ClassA1 classA1 = new ClassA1();
        classA1.setStringField1("A1");
        classA1.setBooleanField1(true);
        classA1.setLongField1(10000L);

        a.setClassA1(classA1);
        ClassB b = dozerMapper.mapAtoB(a);

        assertThat(a.getStringField(), is(equalTo(b.getStringField())));
        System.out.println(a);
        System.out.println(b);
        assertThat(a.isBooleanField(), is(equalTo(b.isBooleanField())));
        assertThat(a.getLongField(), is(equalTo(b.getLongField())));
    }
}